"""Data to keep between HTTP requests (app state)
* selected: list of selected videos
* selectable: list of selectable videos
"""

selected = []
selectable = []

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Django YouTube (version 2)</h1>
    <h2>Seleccionados</h2>
      <ul>
      {selected}
      </ul>
    <h2>Seleccionables</h2>
      <ul>
      {selectable}
      </ul>
  </body>
</html>
"""

VIDEO = """
      <li>
        <form action='/' method='post'>
          <a href='{id}'>{title}</a>
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'>
          <input type='submit' value='{action}'>
        </form>
      </li>
"""

INFO = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <div align="center">
        <h2><a href='http://localhost:8000/'>pagina principal</a></h2>
        <h2>Titulo del video: <a href='{video[link]}'>{video[title]}</a></h2>
        <h2><a href='{video[link]}'><img src={video[image]}></a></h2>
        <h3>Nombre del canal: <a href='{video[urlcanal]}'>{video[canalname]}</a></h2>
        <h4>Fecha de publicación: {video[date]}</h2>
        <h4>Descripción: {video[description]}</h2>
    </body>
</html>
"""

NOT_INFO = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <div align="left">
        <h3>Para acceder al video haga click en seleccionar en la <a href='http://localhost:8000/'>pagina principal</a></h3>
    </body>
</html>
"""
